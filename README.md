# Acer-VX15-Hackintosh

#### 介绍
宏碁 Acer 暗影骑士3进阶版 (an515-52)黑苹果efi------clover5100

#### 对 0.58 开发版的OC进行支持

    经过我的不懈尝试，终于在1.3.8的weg发布之际解决了我的核显无法驱动的问题，之后便开始着手转战OC
    
    现在已经达到与clover同样完美的效果，同时也是为了适应发展，因为liu全家已经不再在clover上进行兼容
    
    性测试，未来应该属于OpenCore的！

#### 电脑配置
电脑型号|宏碁 Acer 暗影骑士3进阶版 (an515-52)
---|---
操作系统|macOS Catalina 15.2（15.3）
处理器|Intel(R) Core(TM) i5 8300H
内存|16 GB DDR4 2400MHz
主板|HM370
显卡|UHD630
声卡|ALC255
网卡|dw1820a


#### 目前实现的效果
1. 注入X86              OK
2. 触摸板，键盘      OK  （闲着没事仿冒了apple鼠标，需要自行更改VirtualSMC.kext-->Info.plist-->IOKitPersonalities-->WiredMouse-1 and WiredMouseAccel-1--> idProduct idVendor改成自己鼠标的，转换成16进制）
3. 声卡注入31         OK
4. 原生电源管理      OK
1. USB定制             OK （5GB/s）
1. 打了0D/6D补丁   OK
1. 睡眠唤醒             这项有待争议，据说HM370没有原生NVRAM，不能深度睡眠，所以我测试发现，既是他睡着了，还是一晚上能耗7%的电量，用监控软件查看中途并未醒来，在终端输入命令查看发现在刚睡眠的时候会出现XDCI，我也很纳闷，但的确睡眠了，希望有大神能够指点一二
1. 原生亮度             OK，并且可以保存上次的亮度
外接键盘在键盘设置可以调节快捷键，我用的F1，F2
1. 变频                     OK
1. 屏蔽独显，减少发热
1. Siri                    OK  
1. 1080p开启hidip
sh -c "$(curl -fsSL https://raw.githubusercontent.com/xzhih/one-key-hidpi/master/hidpi.sh)"
1. 更换dw1820a，怎么说呐，蓝牙是次了点，但可以接力隔空，还不错，屏蔽后二针脚
1. FaceTime iMessage若提示顾客代码，打客服电话即可解决


#### 使用说明
- 直接clone到本地替换

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
